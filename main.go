package main

import (
	"net/http"
	"fmt"
	"log"
	"encoding/json"
)

type Article struct{
	Title string `json:"title"`
	Body string `json:"body"`
}

type Articles []Article

var articles = Articles {
	Article{ Title: "judul 1", Body: "Body1"},
	Article{ Title: "judul 2", Body: "Body2"},
}

func main() {
	http.HandleFunc("/", getHome)
	http.HandleFunc("/articles", getArticles)
	http.HandleFunc("/new-article", withLog(postArticle) )
	http.ListenAndServe(":3000", nil)
}

func getHome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Halo Golang"))
}

func getArticles(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(articles)
}

func postArticle(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		var article Article
		err := json.NewDecoder(r.Body).Decode(&article)

		if err != nil {
			fmt.Println("error ", err)
		}

		articles = append(articles, article)

		json.NewEncoder(w).Encode(articles)
	} else {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
	}
}

func withLog(next http.HandlerFunc) http.HandlerFunc {
	return func (w http.ResponseWriter, r *http.Request) {
		log.Printf("log koneksi dari", r.RemoteAddr)
		next.ServeHTTP(w, r)
	}
}